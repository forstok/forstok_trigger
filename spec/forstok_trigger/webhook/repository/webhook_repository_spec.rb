# frozen_string_literal: true

require 'forstok_trigger/webhook/repository/webhook_repository'
RSpec.describe ForstokTrigger::Webhook::Repository::WebhookRepository do
  describe '.webhook_list_query_join' do
    it 'return query join for webhooks' do
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_list).and_return('list')
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_event).and_return('event')
      expect(subject.webhook_list_query_join).to eql(' FROM list JOIN event on list.event_id=event.id ')
      subject.webhook_list_query_join
    end
  end
  describe '.webhook_list_query_select' do
    it 'return query select for webhooks' do
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_list).and_return('list')
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_event).and_return('event')
      expect(subject.webhook_list_query_select).to eql('SELECT list.id, list.url, event.name  FROM list JOIN event on list.event_id=event.id ')
      subject.webhook_list_query_select
    end
  end
  describe '.webhook_list_query_condition' do
    it 'return query condition for webhooks' do
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_list).and_return('list')
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:configuration_event).and_return('event')
      expect(subject.webhook_list_query_condition('event', 1)).to eq("WHERE list.channel_id=1 AND event.name like 'event'")
      subject.webhook_list_query_condition('event', 1)
    end
  end
  describe '.webhook_list' do
    let(:sql) { double 'query insert' }
    it 'return webhooks' do
      allow(ForstokTrigger::Webhook::Repository).to receive_message_chain(:client, :query).and_return('array')
      expect(ForstokTrigger::Webhook::Repository.client.query(sql)).to eq('array')
    end
  end
  describe '.webhook_insert_query' do
    it 'return query insert for webhooks' do
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive_message_chain(:configuration, :db).and_return('db')
      allow(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive_message_chain(:configuration, :buffer_table).and_return('buffer_table')
      expect(subject.webhook_insert_query).to eql('INSERT INTO db.buffer_table(url, event, event_payload, channel_id) VALUES ')
      subject.webhook_insert_query
    end
  end
  describe '.webhook_insert_value' do
    it 'return query insert for webhooks' do
      expect(subject.webhook_insert_value('event', 'https://google.com', 2, 1)).to eq("('https://google.com', 'event', \"2\\\", 1)")
      subject.webhook_insert_value('event', 'https://google.com', 2, 1)
    end
  end
  describe '.webhook_insert_pending' do
    let(:sql) { double 'query insert' }
    it 'insert webhook' do
      allow(ForstokTrigger::Webhook::Repository).to receive_message_chain(:client, :query).and_return('object')
      expect(ForstokTrigger::Webhook::Repository.client.query(sql)).to eq('object')
    end
  end
end
