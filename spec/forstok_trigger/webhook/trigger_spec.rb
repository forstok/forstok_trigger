# frozen_string_literal: true

require 'forstok_trigger/webhook/trigger'
RSpec.describe ForstokTrigger::Webhook::Trigger do
  describe '.get_webhooks' do
    it 'call webhook_list in trigger_repository to get webhook list' do
      expect(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:webhook_list).with('event',1)
      subject.get_webhooks('event',1)
    end
  end
  describe '.perform' do
    let(:webhook) { double 'webhook' }
    let(:webhook_list) { [webhook] }
    it 'call new in configuration and insert to buffer_table' do
      allow(webhook).to receive(:[]).with('name')
      allow(webhook).to receive(:[]).with('url')
      allow(subject).to receive(:get_webhooks).with('a', 2).and_return(webhook_list)
      expect(ForstokTrigger::Webhook::Repository::WebhookRepository).to receive(:webhook_insert_pending).with(for: 'webhooks').with(webhook['name'], webhook['url'], 1, 2)
      subject.perform('a',1,2)
    end
  end
end
