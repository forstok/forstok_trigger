# frozen_string_literal: true

require 'forstok_trigger/validity/repository/validity_repository'
RSpec.describe ForstokTrigger::Validity::Repository::ValidityRepository do
  describe '.validity_insert_query' do
    it 'return query insert for validity' do
      allow(ForstokTrigger::Validity::Repository::ValidityRepository).to receive_message_chain(:configuration, :db).and_return('db')
      allow(ForstokTrigger::Validity::Repository::ValidityRepository).to receive_message_chain(:configuration, :buffer_table).and_return('buffer_table')
      expect(subject.validity_insert_query).to eql('INSERT INTO db.buffer_table(listing_id, account_id, channel_id, mode, created_at, updated_at) VALUES ')
      subject.validity_insert_query
    end
  end
  describe '.validity_insert_value' do
    it 'return query insert for validity' do
      expect(subject.validity_insert_value(1, 1, 1, 'validity')).to eq("(1, 1, 1, 'validity', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "')")
      subject.validity_insert_value(1, 1, 1, 'validity')
    end
  end
  describe '.validity_insert_pending' do
    let(:sql) { double 'query insert' }
    it 'insert validity' do
      allow(ForstokTrigger::Validity::Repository).to receive_message_chain(:client, :query).and_return('object')
      expect(ForstokTrigger::Validity::Repository.client.query(sql)).to eq('object')
    end
  end
end
