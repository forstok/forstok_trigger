# frozen_string_literal: true

require 'forstok_trigger/export/trigger'
RSpec.describe ForstokTrigger::Export::Trigger do
  describe '.perform' do
    let(:listing_id) { double 1 }
    let(:account_id) { double 1 }
    let(:channel_id) { double 1 }
    let(:mode) { double 'export' }
    it 'call new in configuration and insert to buffer_table' do
      expect(ForstokTrigger::Export::Repository::ExportRepository).to receive(:export_insert_pending).with(
        listing_id,
        account_id,
        channel_id,
        mode
      )
      subject.perform(
        listing_id,
        account_id,
        channel_id,
        mode
      )
    end
  end
end
