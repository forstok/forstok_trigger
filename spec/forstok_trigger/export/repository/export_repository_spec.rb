# frozen_string_literal: true

require 'forstok_trigger/export/repository/export_repository'
RSpec.describe ForstokTrigger::Export::Repository::ExportRepository do
  describe '.export_insert_query' do
    it 'return query insert for export' do
      allow(ForstokTrigger::Export::Repository::ExportRepository).to receive_message_chain(:configuration, :db).and_return('db')
      allow(ForstokTrigger::Export::Repository::ExportRepository).to receive_message_chain(:configuration, :buffer_table).and_return('buffer_table')
      expect(subject.export_insert_query).to eql('INSERT INTO db.buffer_table(listing_id, account_id, channel_id, mode, created_at, updated_at) VALUES ')
      subject.export_insert_query
    end
  end
  describe '.export_insert_value' do
    it 'return query insert for export' do
      expect(subject.export_insert_value(1, 1, 1, 'export')).to eq("(1, 1, 1, 'export', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "')")
      subject.export_insert_value(1, 1, 1, 'export')
    end
  end
  describe '.export_insert_pending' do
    let(:sql) { double 'query insert' }
    it 'insert export' do
      allow(ForstokTrigger::Export::Repository).to receive_message_chain(:client, :query).and_return('object')
      expect(ForstokTrigger::Export::Repository.client.query(sql)).to eq('object')
    end
  end
end
