# frozen_string_literal: true

require 'forstok_trigger/qc/repository'
RSpec.describe ForstokTrigger::Qc::Repository do
  describe '.client' do
    it 'call client in repository to connect database' do
      allow(ForstokTrigger::Qc).to receive_message_chain(:configuration, :host).and_return('host')
      allow(ForstokTrigger::Qc).to receive_message_chain(:configuration, :port).and_return('port')
      allow(ForstokTrigger::Qc).to receive_message_chain(:configuration, :username).and_return('username')
      allow(ForstokTrigger::Qc).to receive_message_chain(:configuration, :password).and_return('password')
      allow(ForstokTrigger::Qc).to receive_message_chain(:configuration, :db).and_return('db')
      expect(Mysql2::Client).to receive(:new).with(
        host: 'host',
        port: 'port',
        username: 'username',
        password: 'password',
        database: 'db'
      )
      subject.client
    end
  end
end
