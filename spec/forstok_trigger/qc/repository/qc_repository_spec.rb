# frozen_string_literal: true

require 'forstok_trigger/qc/repository/qc_repository'
RSpec.describe ForstokTrigger::Qc::Repository::QcRepository do
  describe '.qc_insert_query' do
    it 'return query insert for qc' do
      allow(ForstokTrigger::Qc::Repository::QcRepository).to receive_message_chain(:configuration, :db).and_return('db')
      allow(ForstokTrigger::Qc::Repository::QcRepository).to receive_message_chain(:configuration, :buffer_table).and_return('buffer_table')
      expect(subject.qc_insert_query).to eql('INSERT INTO db.buffer_table(listing_id, account_id, channel_id, mode, created_at, updated_at) VALUES ')
      subject.qc_insert_query
    end
  end
  describe '.qc_insert_value' do
    it 'return query insert for qc' do
      expect(subject.qc_insert_value(1, 1, 1, 'qc')).to eq("(1, 1, 1, 'qc', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "', '" + Time.now.strftime("%Y-%m-%d %H:%M:%S") + "')")
      subject.qc_insert_value(1, 1, 1, 'qc')
    end
  end
  describe '.qc_insert_pending' do
    let(:sql) { double 'query insert' }
    it 'insert qc' do
      allow(ForstokTrigger::Qc::Repository).to receive_message_chain(:client, :query).and_return('object')
      expect(ForstokTrigger::Qc::Repository.client.query(sql)).to eq('object')
    end
  end
end
