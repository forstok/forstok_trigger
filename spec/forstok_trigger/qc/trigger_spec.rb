# frozen_string_literal: true

require 'forstok_trigger/qc/trigger'
RSpec.describe ForstokTrigger::Qc::Trigger do
  describe '.perform' do
    let(:listing_id) { double 1 }
    let(:account_id) { double 1 }
    let(:channel_id) { double 1 }
    let(:mode) { double 'qc' }
    it 'call new in configuration and insert to buffer_table' do
      expect(ForstokTrigger::Qc::Repository::QcRepository).to receive(:qc_insert_pending).with(
        listing_id,
        account_id,
        channel_id,
        mode
      )
      subject.perform(
        listing_id,
        account_id,
        channel_id,
        mode
      )
    end
  end
end
