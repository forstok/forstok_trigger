# frozen_string_literal: true

require 'forstok_trigger/webhook'
RSpec.describe ForstokTrigger::Webhook do
  describe '.configuration' do
    it 'call new in configuration' do
      expect(ForstokTrigger::Configuration::WebhookConfiguration).to receive(:new)
      subject.configuration
    end
  end

  describe '.reset' do
    it 'call new in configuration' do
      expect(ForstokTrigger::Configuration::WebhookConfiguration).to receive(:new)
      subject.reset
    end
  end

  describe '.configure' do
    let(:configuration) { double 'configuration' }
    it 'call yield in configuration' do
      allow(ForstokTrigger::Configuration::WebhookConfiguration).to receive(:new).and_return(configuration)
      allow(configuration).to receive(:db=)
      allow(subject).to receive(:yield).with(configuration)
      subject.configure do |config|
        configuration.db = 'test_db'
      end
    end
  end
end
