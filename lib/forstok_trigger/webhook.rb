# frozen_string_literal: true

require 'forstok_trigger/webhook/trigger.rb'
require 'forstok_trigger/webhook/repository/webhook_repository.rb'
require 'forstok_trigger/configuration/webhook_configuration.rb'
module ForstokTrigger
  # Class Trigger is main class for run perform
  module Webhook
    def self.configuration
      @configuration ||= ForstokTrigger::Configuration::WebhookConfiguration.new
    end

    def self.reset
      @configuration = ForstokTrigger::Configuration::WebhookConfiguration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end
