# frozen_string_literal: true

require 'forstok_trigger/qc/repository/qc_repository.rb'
module ForstokTrigger
  module Qc
    # Module Trigger is main module for run perform qc trigger
    module Trigger
      def self.perform(listing_id, account_id, channel_id, mode)
        ForstokTrigger::Qc::Repository::QcRepository
          .qc_insert_pending(
            listing_id,
            account_id,
            channel_id,
            mode
          )
      end
    end
  end
end
