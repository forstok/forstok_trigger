# frozen_string_literal: true

module ForstokTrigger
  module Qc
    module Repository
      # This TriggerRepository is class for connect to database
      module QcRepository
        def self.qc_insert_query
          sql = 'INSERT INTO ' + configuration.db + '.'
          sql += configuration.buffer_table
          sql += '(listing_id, account_id, channel_id, mode, created_at'
          sql += ', updated_at) VALUES '
          sql
        end

        def self.qc_insert_value(listing_id, account_id, channel_id, mode)
          "(#{listing_id}, #{account_id}, #{channel_id}, " \
          "'#{mode}', '#{now}', '#{now}')"
        end

        def self.qc_insert_pending(listing_id, account_id, channel_id, mode)
          sql = qc_insert_query
          sql += qc_insert_value(listing_id, account_id, channel_id, mode)
          ForstokTrigger::Qc::Repository.client.query(sql)
        end

        def self.now
          @now = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        end

        def self.configuration
          @configuration ||= ForstokTrigger::Qc.configuration
        end
      end
    end
  end
end
