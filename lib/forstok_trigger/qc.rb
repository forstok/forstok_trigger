# frozen_string_literal: true

require 'forstok_trigger/qc/trigger.rb'
require 'forstok_trigger/qc/repository/qc_repository.rb'
require 'forstok_trigger/configuration/qc_configuration.rb'
module ForstokTrigger
  # Class Trigger is main class for run perform
  module Qc
    def self.configuration
      @configuration ||= ForstokTrigger::Configuration::QcConfiguration.new
    end

    def self.reset
      @configuration = ForstokTrigger::Configuration::QcConfiguration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end
