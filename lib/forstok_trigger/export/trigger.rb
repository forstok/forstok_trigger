# frozen_string_literal: true

require 'forstok_trigger/export/repository/export_repository.rb'
module ForstokTrigger
  module Export
    # Module Trigger is main module for run perform export trigger
    module Trigger
      def self.perform(listing_id, account_id, channel_id, mode)
        ForstokTrigger::Export::Repository::ExportRepository
          .export_insert_pending(
            listing_id,
            account_id,
            channel_id,
            mode
          )
      end
    end
  end
end
