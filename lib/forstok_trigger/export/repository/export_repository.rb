# frozen_string_literal: true

module ForstokTrigger
  module Export
    module Repository
      # This TriggerRepository is class for connect to database
      module ExportRepository
        def self.export_insert_query
          sql = 'INSERT INTO ' + configuration.db + '.'
          sql += configuration.buffer_table
          sql += '(listing_id, account_id, channel_id, mode, created_at'
          sql += ', updated_at) VALUES '
          sql
        end

        def self.export_insert_value(listing_id, account_id, channel_id, mode)
          "(#{listing_id}, #{account_id}, #{channel_id}, " \
          "'#{mode}', '#{now}', '#{now}')"
        end

        def self.export_insert_pending(listing_id, account_id, channel_id, mode)
          sql = export_insert_query
          sql += export_insert_value(listing_id, account_id, channel_id, mode)
          ForstokTrigger::Export::Repository.client.query(sql)
        end

        def self.now
          @now = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        end

        def self.configuration
          @configuration ||= ForstokTrigger::Export.configuration
        end
      end
    end
  end
end
