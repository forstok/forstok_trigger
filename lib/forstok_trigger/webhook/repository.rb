# frozen_string_literal: true

require 'mysql2'
module ForstokTrigger
  module Webhook
    # Class Trigger is main class for run perform
    module Repository
      def self.client
        Mysql2::Client.new(
          host: configuration.host,
          port: configuration.port,
          username: configuration.username,
          password: configuration.password,
          database: configuration.db
        )
      end

      def self.configuration
        @configuration ||= ForstokTrigger::Webhook.configuration
      end
    end
  end
end
