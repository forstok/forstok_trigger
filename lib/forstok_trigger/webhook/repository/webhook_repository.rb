# frozen_string_literal: true

module ForstokTrigger
  module Webhook
    module Repository
      # This TriggerRepository is class for connect to database
      module WebhookRepository
        def self.webhook_list_query_select
          sql = 'SELECT ' + configuration_list + '.id, '
          sql += configuration_list + '.url, '
          sql += configuration_event + '.name '
          sql += webhook_list_query_join
          sql
        end

        def self.webhook_list_query_join
          sql = ' FROM ' + configuration_list
          sql += ' JOIN ' + configuration_event
          sql += ' on ' + configuration_list + '.event_id='
          sql += configuration_event + '.id '
          sql
        end

        def self.webhook_list_query_condition(event, channel_id)
          sql = 'WHERE ' + configuration_list
          sql += '.channel_id=' + channel_id.to_s
          sql += ' AND ' + configuration_event
          sql += ".name like '" + event.to_s + "'"
          sql
        end

        def self.webhook_list(event, channel_id)
          sql = webhook_list_query_select
          sql += webhook_list_query_condition(event, channel_id)
          results = ForstokTrigger::Webhook::Repository.client.query(sql)
          results
        end

        def self.webhook_insert_query
          sql = 'INSERT INTO ' + configuration.db + '.'
          sql += configuration.buffer_table
          sql += '(url, event, event_payload, channel_id) VALUES '
          sql
        end

        def self.webhook_insert_value(event, url, event_payload, channel_id)
          sql = "('" + url.to_s + "', '" + event.to_s + "', '"
          sql += Mysql2::Client.escape(event_payload.to_s) + "', "
          sql += channel_id.to_s + ')'
          sql
        end

        def self.webhook_insert_pending(event, url, event_payload, channel_id)
          sql = webhook_insert_query
          sql += webhook_insert_value(event, url, event_payload, channel_id)
          ForstokTrigger::Webhook::Repository.client.query(sql)
        end

        def self.configuration_list
          configuration.list
        end

        def self.configuration_event
          configuration.event
        end

        def self.configuration
          @configuration ||= ForstokTrigger::Webhook.configuration
        end
      end
    end
  end
end
