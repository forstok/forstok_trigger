# frozen_string_literal: true

require 'forstok_trigger/webhook/repository/webhook_repository.rb'
module ForstokTrigger
  module Webhook
    # Module Trigger is main module for run perform webhook trigger
    module Trigger
      def self.perform(event, event_payload, channel_id)
        webhooks = get_webhooks(event, channel_id)
        webhooks.each do |webhook|
          ForstokTrigger::Webhook::Repository::WebhookRepository
            .webhook_insert_pending(
              webhook['name'],
              webhook['url'],
              event_payload,
              channel_id
            )
        end
      end

      def self.get_webhooks(event, channel_id)
        ForstokTrigger::Webhook::Repository::WebhookRepository
          .webhook_list(event, channel_id)
      end
    end
  end
end
