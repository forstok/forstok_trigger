# frozen_string_literal: true

require 'forstok_trigger/validity/trigger.rb'
require 'forstok_trigger/validity/repository/validity_repository.rb'
require 'forstok_trigger/configuration/valid_configuration.rb'
module ForstokTrigger
  # Class Trigger is main class for run perform
  module Validity
    def self.configuration
      @configuration ||= ForstokTrigger::Configuration::ValidConfiguration.new
    end

    def self.reset
      @configuration = ForstokTrigger::Configuration::ValidConfiguration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end
