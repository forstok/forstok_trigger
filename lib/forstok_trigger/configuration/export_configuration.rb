# frozen_string_literal: true

module ForstokTrigger
  class Configuration
    # This Configuration class is for initialize config to database
    class ExportConfiguration < Configuration
      attr_accessor :buffer_table
    end
  end
end
