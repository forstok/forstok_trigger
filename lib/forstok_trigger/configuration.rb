# frozen_string_literal: true

module ForstokTrigger
  # This Configuration class is for initialize config to database
  class Configuration
    attr_accessor :db, :host, :port, :username, :password
  end
end
