# frozen_string_literal: true

module ForstokTrigger
  module Validity
    module Repository
      # This TriggerRepository is class for connect to database
      module ValidityRepository
        def self.validity_insert_query
          sql = 'INSERT INTO ' + configuration.db + '.'
          sql += configuration.buffer_table
          sql += '(listing_id, account_id, channel_id, mode, created_at'
          sql += ', updated_at) VALUES '
          sql
        end

        def self.validity_insert_value(listing_id, account_id, channel_id, mode)
          "(#{listing_id}, #{account_id}, #{channel_id}, " \
          "'#{mode}', '#{now}', '#{now}')"
        end

        def self.validity_insert_pending(l_id, a_id, c_id, mode)
          sql = validity_insert_query
          sql += validity_insert_value(l_id, a_id, c_id, mode)
          ForstokTrigger::Validity::Repository.client.query(sql)
        end

        def self.now
          @now = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        end

        def self.configuration
          @configuration ||= ForstokTrigger::Validity.configuration
        end
      end
    end
  end
end
