# frozen_string_literal: true

require 'forstok_trigger/validity/repository/validity_repository.rb'
module ForstokTrigger
  module Validity
    # Module Trigger is main module for run perform validity trigger
    module Trigger
      def self.perform(listing_id, account_id, channel_id, mode)
        ForstokTrigger::Validity::Repository::ValidityRepository
          .validity_insert_pending(
            listing_id,
            account_id,
            channel_id,
            mode
          )
      end
    end
  end
end
