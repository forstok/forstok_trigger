# frozen_string_literal: true

require 'forstok_trigger/export/trigger.rb'
require 'forstok_trigger/export/repository/export_repository.rb'
require 'forstok_trigger/configuration/export_configuration.rb'
module ForstokTrigger
  # Class Trigger is main class for run perform
  module Export
    def self.configuration
      @configuration ||= ForstokTrigger::Configuration::ExportConfiguration.new
    end

    def self.reset
      @configuration = ForstokTrigger::Configuration::ExportConfiguration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end
