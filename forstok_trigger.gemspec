# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'forstok_trigger/version'

Gem::Specification.new do |spec|
  spec.name          = 'forstok_trigger'
  spec.version       = ForstokTrigger::VERSION
  spec.authors       = ['Kevin Ivander']
  spec.email         = ['kevin.ivander@forstok.com']

  spec.summary       = 'Forstok Trigger'
  spec.description   = 'Forstok Gem trigger in microservice'
  # spec.homepage      = 'https://rubygems.org/gems/'
  spec.license       = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'

    # spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://bitbucket.org/forstok/forstok_trigger/src/master'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  spec.files = [
    # Dir.chdir(File.expand_path(__dir__)) do
    #   `git ls-files -z`
    #     .split('\x0').reject { |f| f.match(%r{^(test|spec|features)/}) }
    # end,
    Dir['lib/**/*.rb']
  ]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.1.4'
  spec.add_development_dependency 'byebug', '~> 11.1.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.79.0'
  spec.add_dependency 'mysql2', '~> 0.5.3'
end
